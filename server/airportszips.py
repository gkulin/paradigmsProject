import json

f = open('airports_old.json')
orig = json.load(f)

a_dict = {"ATL":"30320", "BOS":"02128", "BWI":"21240", "CLT":"28208", "DCA":"22202", "DEN":"80249", "DFW":"75261", "DTW":"48242", "EWR":"07114", "FLL":"33315", "IAD":"20166", "IAH":"77032", "JFK":"11430", "LAS":"89119", "LAX":"90045", "LGA":"11371", "MCO":"32827", "MDW":"60638", "MIA":"33142", "MSP":"55450", "ORD":"60666", "PDX":"97218",  "PHL":"19153", "PHX":"85034", "SAN":"92101", "SEA":"98158", "SFO":"94128", "SLC":"84122", "TPA":"33607"}

count = 0

newlist = []

for a in orig:
    for z in a_dict:
        if a['Airport']['Code'] == z:
            orig[count]['Airport'].update({"Zipcode":a_dict[z]})
            count+=1

nf = open("airports.json", "w")
json.dump(orig, nf)

#nf = open('airports_new.json')
