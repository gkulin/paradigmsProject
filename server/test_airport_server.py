import unittest
import requests
import json

class TestCherrypyPrimer(unittest.TestCase):

    SITE_URL = 'http://localhost:51034' #'http://student04.cse.nd.edu:510XX'
    DICT_URL = SITE_URL + '/airports/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_get_all(self):
        airports = ['ATL', 'BOS', 'BWI', 'CLT', 'DCA', 'DEN', 'DFW', 'DTW', 'EWR', 'FLL', 'IAD', 'IAH', 'JFK', 'LAS', 'LAX', 'LGA', 'MCO', 'MDW', 'MIA', 'MSP', 'ORD', 'PDX', 'PHL', 'PHX', 'SAN', 'SEA', 'SFO', 'SLC', 'TPA']

        r = requests.get(self.DICT_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['entries'], airports)


    def test_get_label(self):
        airport = 'ATL'
        yr = '2003'
        mo = '6'
        r = requests.get(self.DICT_URL + airport + '/' + yr + '/' + mo)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['Average Delay'], 46.0)

    def test_get_months(self):
        airport = 'ATL'
        r = requests.get(self.DICT_URL + airport)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['Average Delays by Month']['January'], 59.3)

    def test_post_airport(self):
        airport = "SBN"
        yr = "2020"
        mo = "1"
        m = {}
        m['Airport'] = {"Code": "SBN", "Name": "South Bend, IN: South Bend International"}
        m['Time'] = {"Label": "2020/01", "Month": 1, "Month Name": "January", "Year": 2020}
        m['Statistics'] = {"# of Delays": {"Carrier": 1009, "Late Aircraft": 1275, "National Aviation System": 3217, "Security": 17,"Weather": 328},"Carriers": {"Names": "Allegiant, American Airlines, Delta, United","Total": 4}, "Flights": {"Cancelled": 2,"Delayed": 10,"Diverted": 3,"On Time": 1000,"Total": 1015}, "Minutes Delayed": {"Carrier": 500, "Late Aircraft": 700, "National Aviation System": 200, "Security": 40, "Total": 1740, "Weather": 300}}

        r = requests.post(self.DICT_URL + airport, data = json.dumps(m)) #performing post
        self.assertTrue(self.is_json(r.content.decode()))   # 'false is not true'
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DICT_URL + airport + '/' + yr + '/' + mo) # uses get
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        self.assertEqual(resp['Average Delay'], 174.0)


    def test_put_label(self):
        airport = 'ATL'
        yr = '2003'
        mo = '6'

        m = {}
        m['Minutes Delayed'] = {"Carrier": 61606, "Late Aircraft": 68335, "National Aviation System": 118831, "Security": 754, "Total": 269000, "Weather": 19474}

        r = requests.put(self.DICT_URL + airport + '/' + yr + '/' + mo, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        r = requests.get(self.DICT_URL + airport + '/' + yr + '/' + mo)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success') 


    def test_delete_label(self):
        airport = 'ATL'
        yr = '2006'
        mo = '6'
        r = requests.delete(self.DICT_URL + airport + '/' + yr + '/' + 'mo')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.DICT_URL + airport + '/' + yr + '/' + 'mo')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'error')


if __name__ == "__main__":
    unittest.main()
