import routes
import cherrypy
from airports_controller import AirportsController

def start_service():
    #controller object
    a_cont = AirportsController()

    #set up dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    #connect get /airports/
    dispatcher.connect('get_all_aiports', '/airports/', controller=a_cont, action ='GET_ALL', conditions=dict(method=['GET']))

    #connect get /airports/zip/:code
    dispatcher.connect('get_airline', '/airports/airline/:airline', controller=a_cont, action ='GET_AIRLINE', conditions=dict(method=['GET']))

    #connect get /airports/zip/:code
    dispatcher.connect('get_zip', '/airports/zip/:code', controller=a_cont, action ='GET_ZIP', conditions=dict(method=['GET']))

    #connect get /airports/:airport/:yr/:mo
    dispatcher.connect('get_by_label', '/airports/:airport/:yr/:mo', controller=a_cont, action ='GET_LABEL', conditions=dict(method=['GET']))

    #connect get /airports/:airport
    dispatcher.connect('get_month_averages', '/airports/:airport', controller=a_cont, action ='GET_MONTHS', conditions=dict(method=['GET']))

    #connect get /airports/:airport
    dispatcher.connect('get_total_average', '/airports/:airport/average', controller=a_cont, action ='GET_AVERAGE', conditions=dict(method=['GET']))


    #connect post /airports/:airport
    dispatcher.connect('post_airport', '/airports/:airport', controller=a_cont, action ='POST_AIRPORT', conditions=dict(method=['POST']))

    #connect put /airports/:airport/:yr/:mo
    dispatcher.connect('put_airport', '/airports/:airport/:yr/:mo', controller=a_cont, action ='PUT_LABEL', conditions=dict(method=['PUT']))

    #connect delete /airports/:airport/:yr/:mo
    dispatcher.connect('delete_airport_by_label', '/airports/:airport/:yr/:mo', controller=a_cont, action ='DELETE_LABEL', conditions=dict(method=['DELETE']))

    # default OPTIONS handler for CORS, all direct to the same place
    dispatcher.connect('dict_options', '/airports/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('dict_zip_options', '/airports/zip/:code', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('dict_avg_options', '/airports/:airport/average', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))



    #configure server
    conf = {
        'global': {
            'server.socket_host': 'localhost', #student04.cse.nd.edu
            'server.socket_port': 51034 #51000+your mid from hw6
            },
        '/': {
            'request.dispatch' : dispatcher, #object dispatcher
            'tools.CORS.on' : True, # configuration for CORS
        }
    }

    #update cond with cherrypy
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)

    #start server
    cherrypy.quickstart(app)

# class for CORS
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
    start_service()
