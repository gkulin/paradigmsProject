#controller class with event handlers
import cherrypy
import json

class AirportsController(object):

    def __init__(self):
        f = open('airports.json')
        self.myd = json.load(f) #load data from data file into dictionary

    def GET_ALL(self):
        # set up default response
        output_json = {'result' : 'success'}
        airports = []
        for a in self.myd:
            if a['Airport']['Code'] not in airports:
                airports.append(a['Airport']['Code'])
        output_json['entries'] = airports
        return json.dumps(output_json)

    def GET_ZIP(self, code):
        output_json = {'result' : 'success'}
        for a in self.myd:
            if a['Airport']['Code'] == code:
                output_json['Zipcode'] = a['Airport']['Zipcode']
                return json.dumps(output_json)
        output_json = {'result' : 'error'}
        return json.dumps(output_json)

    def GET_AIRLINE(self, airline):
        output_json = {'result' : 'success'}
        airports = []
        for a in self.myd:
            if (airline in a['Statistics']['Carriers']['Names']) and (a['Airport']['Code'] not in airports):
                airports.append(a['Airport']['Code'])
        if len(airports) > 0:
            output_json['Airports'] = airports
        else:
            output_json['result'] = 'error'
        return json.dumps(output_json)


    def GET_LABEL(self,ad_id=None,*args, **kwargs):
        # set up default response
        output_json = {'result' : 'error'}

        for a in self.myd:
            if (a['Airport']['Code'] == kwargs['airport']) and (str(a['Time']['Year']) == kwargs['yr']) and (str(a['Time']['Month']) == kwargs['mo']):
                output_json['result'] = 'success'
                print(a['Statistics']['Minutes Delayed'])
                output_json['Average Delay'] = round(a['Statistics']['Minutes Delayed']['Total'] / a['Statistics']['Flights']['Delayed'], 1)
        return json.dumps(output_json)

    def GET_MONTHS(self, airport):
        # set up default response
        output_json = {'result' : 'error'}
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

        entries = {}

        for m in months:
            averages = []
            count = 0
            for a in self.myd:
                if (a['Time']['Month Name']) == m and a['Airport']['Code'] == airport:
                    output_json['result'] = 'success'
                    count = count + 1
                    if a['Statistics']['Flights']['Delayed'] != 0:
                        averages.append(a['Statistics']['Minutes Delayed']['Total'] / a['Statistics']['Flights']['Delayed'])
            avgsum = 0
            for avg in averages:
                avgsum += avg
            if count != 0:
                entries[m] = round(avgsum/count, 1)

            output_json['Average Delays by Month'] = entries

        return json.dumps(output_json)


    def GET_AVERAGE(self, airport):
        # set up default response
        output_json = {'result' : 'success'}
        months_json = {}
        months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

        entries = {}
        average = 0

        for m in months:
            averages = []
            count = 0
            for a in self.myd:
                if (a['Time']['Month Name']) == m and a['Airport']['Code'] == airport:
                    output_json['result'] = 'success'
                    count = count + 1
                    if a['Statistics']['Flights']['Delayed'] != 0:
                        averages.append(a['Statistics']['Minutes Delayed']['Total'] / a['Statistics']['Flights']['Delayed'])
            avgsum = 0
            for avg in averages:
                avgsum += avg
            if count != 0:
                monthAvg = round(avgsum/count, 1)
                average = average + monthAvg

        average = average / 12
        average = str(round(average, 2))

        output_json['Average Delay'] = average

        return json.dumps(output_json)


    def POST_AIRPORT(self, airport):
        # set up default response
        output_json = {'result' : 'success'}

        #get body of message into data
        try:
            data_str = cherrypy.request.body.read()
            data_json = json.loads(data_str)    # this is the message body
            (self.myd).append(data_json)
        except Exception as ex:
            output_json['result'] = 'error'
            output_json['message'] = str(ex)

        return json.dumps(output_json)


    def PUT_LABEL(self,ad_id=None,*args, **kwargs):
        # set up default response
        output_json = {'result' : 'success'}
        #get body of message into data
        try:
            data_str = cherrypy.request.body.read()
            data_json = json.loads(data_str)    # this is the message body

            for a in self.myd:
                if (a['Airport']['Code'] == kwargs['airport']) and (str(a['Time']['Year']) == kwargs['yr']) and (str(a['Time']['Month']) == kwargs['mo']):
                    a['Statistics']['Minutes Delayed'] = data_json['Minutes Delayed']
        except Exception as ex:
            output_json['result'] = 'error'
            output_json['message'] = str(ex)
        return json.dumps(output_json)


    def DELETE_LABEL(self,ad_id=None,*args, **kwargs):
        # set up default response
        output_json = {'result' : 'success'}
        try:
            for a in self.myd:
                if (a['Airport']['Code'] == kwargs['airport']) and (str(a['Time']['Year']) == kwargs['yr']) and (str(a['Time']['Month']) == kwargs['mo']):
                    del a
        except KeyError as ex:  # This is the same as the else statement immediately above
            output_json['result'] = 'error'
            output_json['message'] = str(ex)
        except Exception as ex:
            output_json['result'] = 'error'
            output_json['message'] = str(ex)
        return json.dumps(output_json)
