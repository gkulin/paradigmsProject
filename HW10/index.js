// get info after submit is pressed
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
  console.log('entered getFormInfo!');
  // call displayinfo
  var airportCode = document.getElementById("name-text").value;
  //console.log('Airport code you entered is ' + airportCode);
  makeNetworkCallToAirportApi(airportCode);    // to get zipCode
  //var zipCode = '48242'
  //makeNetworkCallToZipApi(zipCode, airportCode);   // eventually move this to after getting the real zipcode
} // end of get form info

function makeNetworkCallToAirportApi(airportCode) {
  console.log('entered make nw call to airport ' + airportCode);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://localhost:51034/airports/zip/" + airportCode;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateAirportCodeResponse(xhr.responseText, airportCode);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call to airport code

function updateAirportCodeResponse(response_text, airportCode) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label1 = document.getElementById("response-line1");

  if (response_json['result'] == 'error') {
    label1.innerHTML = 'Apologies, we could not find your airport code.';
  } else {
    console.log(response_json['Zipcode']);
    var zipCode = response_json['Zipcode'];
    label1.innerHTML = 'The zipcode for airport ' + airportCode + ' is  ' + zipCode;
    makeNetworkCallToZipApi(zipCode, airportCode);
  }
} // end of updateAirportCodeResponse

function makeNetworkCallToZipApi(zipCode, airportCode) {
  console.log('entered make nw call to zip ' + zipCode);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://api.zippopotam.us/us/" + zipCode;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateZipResponse(zipCode, xhr.responseText, airportCode);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call to zip

function updateZipResponse(zipCode, response_text, airportCode) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label2 = document.getElementById("response-line2");

  if (response_json['places'] == null) {
    label2.innerHTML = 'Apologies, we could not find your zip code.'
  } else {
    console.log(response_json['places'][0])
    label2.innerHTML = 'The latitude of ' + airportCode + ' (at zip code ' + zipCode + ') is ' + response_json['places'][0]['latitude'] + ' and the latitude is ' + response_json['places'][0]['longitude'];
  }
} // end of updateZipResponse
