//window.onload = getAllAirportCodes();

const allAirportLocs = [
  ["ATL", 33.6568, -84.4236],
  ["BOS", 42.3642, -71.0257],
  ["BWI", 39.1753, -76.6732],
  ["CLT", 35.2358, -80.8964],
  ["DCA", 38.8565, -77.0592],
  ["DEN", 39.7783, -104.7556],
  ["DFW", 32.7673, -96.7776],
  ["DTW", 42.2166, -83.3532],
  ["EWR", 40.7082, -74.1891],
  ["FLL", 26.0989, -80.1541],
  ["IAD", 38.9814, -77.4723],
  ["IAH", 29.9368, -95.3299],
  ["JFK", 40.6472, -73.7827],
  ["LAS", 36.1008,-115.1365],
  ["LAX", 33.9631,-118.3941],
  ["LGA", 40.7721, -73.8735],
  ["MCO", 28.4317, -81.343],
  ["MDW", 41.7814, -87.7705],
  ["MIA", 25.813, -80.232],
  ["MSP", 44.8811, -93.2207],
  ["ORD", 41.968, -87.8912],
  ["PDX", 45.56, -122.6001],
  ["PHL", 39.9055, -75.2444],
  ["PHX", 33.4413, -112.0421],
  ["SAN", 32.7185, -117.1593],
  ["SEA",	47.4497, -122.3076],
  ["SFO", 37.6216, -122.3929],
  ["SLC", 40.6681, -111.9083],
  ["TPA", 27.9625, -82.4895],
];

//*****reset functionts*****
//reset airport code
var resetButton = document.getElementById('bsr-clear-button');
resetButton.onmouseup = resetCode;

function resetCode() {
  var label1 = document.getElementById("response-line1");
  label1.innerHTML = '';
  var label2 = document.getElementById("response-line2");
  label2.innerHTML = '';
  var label3 = document.getElementById("response-line3");
  label3.innerHTML = '';
  initMap();
}

//reset airline
var resetButton2 = document.getElementById('bsr-clear-button2');
resetButton2.onmouseup = resetAirline;

function resetAirline() {
  var label4 = document.getElementById("response-line4");
  label4.innerHTML = '';
  initMap();
}

//*****end reset functions

//*****Begin Airline functions****
//get info for airline form when submit is pressed
var submitButton2 = document.getElementById('bsr-submit-button2');
submitButton2.onmouseup = getFormInfo2;

function getFormInfo2() {
  console.log('entered getFormInfo2');
  var airline = document.getElementById("airline-text").value;

  makeNetworkCallToAirlineApi(airline);

}

function makeNetworkCallToAirlineApi(airline) {
  console.log('entered make nw call for airline ' + airline);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://localhost:51034/airports/airline/" + airline;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateAirlineResponse(xhr.responseText, airline);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request
}

function updateAirlineResponse(response_text, airline) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label4 = document.getElementById("response-line4");

  if (response_json['result'] == 'error') {
    label4.innerHTML = 'Apologies, we could not find any airports with the airline ' + airline + '.';
  } else {
    console.log(response_json['Airports']);
    var airports = response_json['Airports'];
    updateMap2(airports);
    for (let i = 0; i < airports.length; i++) {
      airports[i] = '\n' + airports[i]
    }
    label4.innerHTML = 'The airline ' + airline + ' is served by the following airports: ' + airports;
  }
}

function updateMap2(airports) {
  // The location of Denver, the center
  const centerAirport = { lat: 39.7783, lng: -104.7556 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 3.5,
    center: centerAirport,
  });

  for (let i = 0; i < allAirportLocs.length; i++) {
    const airport = allAirportLocs[i];
    for (let j = 0; j < airports.length; j++) {
      if (airport[0] == airports[j]) {
        new google.maps.Marker({
          position: { lat: airport[1], lng: airport[2] },
          map,
          title: airport[0],
        });
      }
    }
  }
}

//*****End Airline functions*****

//******Begin Airport Code functions*****
// get info for airport code form after submit is pressed
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
  console.log('entered getFormInfo!');
  // call displayinfo
  var airportCode = document.getElementById("name-text").value;
  //console.log('Airport code you entered is ' + airportCode);
  makeNetworkCallToAirportApi(airportCode);    // to get zipCode
  makeNetworkCallToAvgApi(airportCode)
} // end of get form info

function makeNetworkCallToAvgApi(airportCode) {
  console.log('entered make nw call to airport ' + airportCode);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://localhost:51034/airports/" + airportCode + "/average";
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateAirportAvgResponse(xhr.responseText, airportCode);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call to airport code

function updateAirportAvgResponse(response_text, airportCode) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label1 = document.getElementById("response-line1");

  if (response_json['result'] == 'error') {
    label1.innerHTML = 'Apologies, we could not find your airport code.';
  } else {
    console.log(response_json['Zipcode']);
    var average = response_json['Average Delay'];
    label1.innerHTML = 'The average delay for ' + airportCode + ' is  ' + average;
  }
} // end of updateAirportCodeResponse

function makeNetworkCallToAirportApi(airportCode) {
  console.log('entered make nw call to airport ' + airportCode);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://localhost:51034/airports/zip/" + airportCode;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateAirportCodeResponse(xhr.responseText, airportCode);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call to airport code

function updateAirportCodeResponse(response_text, airportCode) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label2 = document.getElementById("response-line2");
  var label3 = document.getElementById("response-line3");

  if (response_json['result'] == 'error') {
    label2.innerHTML = 'Apologies, we could not find your airport code.';
    label3.innerHTML = ' ';
  } else {
    console.log(response_json['Zipcode']);
    var zipCode = response_json['Zipcode'];
    //label2.innerHTML = 'The zipcode for airport ' + airportCode + ' is  ' + zipCode;
    makeNetworkCallToZipApi(zipCode, airportCode);
    updateMap(airportCode);
  }
} // end of updateAirportCodeResponse

function makeNetworkCallToZipApi(zipCode, airportCode) {
  console.log('entered make nw call to zip ' + zipCode);

  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://api.zippopotam.us/us/" + zipCode;
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr

  // set up onload
  xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    // do something
    updateZipResponse(zipCode, xhr.responseText, airportCode);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }

  // actually make the network call
  xhr.send(null) // last step - this actually makes the request

} // end of make nw call to zip

function updateZipResponse(zipCode, response_text, airportCode) {
  var response_json = JSON.parse(response_text);
  // update a label
  var label3 = document.getElementById("response-line3");


  if (response_json['places'] == null) {
    label3.innerHTML = 'Apologies, we could not find your zip code.'
  } else {
    console.log(response_json['places'][0])
    label3.innerHTML = 'The latitude of ' + airportCode + ' (at zip code ' + zipCode + ') is ' + response_json['places'][0]['latitude'] + ' and the latitude is ' + response_json['places'][0]['longitude'];
  }
} // end of updateZipResponse

function getAllAirportCodes() {
  console.log("entered get airport codes");
  // set up url
  var xhr = new XMLHttpRequest(); // 1 - creating request object
  var url = "http://localhost:51034/airports/";
  xhr.open("GET", url, true); // 2 - associates request attributes with xhr
  console.log("made it thru xhr.open");
  // set up onload
  console.log(xhr.responseText);
  xhr.onload = function(e) { // triggered when response is received
    console.log("made it into xhr.onload");
    // must be written before send
    console.log(xhr.responseText);
    // do something
    getAllLocations(xhr.responseText);
  }

  // set up onerror
  xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
  }
  // actually make the network call
  xhr.send(null) // last step - this actually makes the request
}

function getAllLocations(response_text) {
  console.log("entered get all locations");
  var response_json = JSON.parse(response_text);
  // update a label
  var label1 = document.getElementById("response-line1");

  if (response_json['result'] == 'error') {
    label1.innerHTML = 'Apologies, we could not find your airport code.';
  } else {
    console.log(response_json['entries']);
    var airports = response_json['entries'];
    console.log(airports);
    //var arrayLength = airports.length;
    //for (var i = 0; i < arrayLength; i++) {
    //}
  }
}

// Initialize and add the map
function initMap() {
  // The location of Denver, the center
  const centerAirport = { lat: 39.7783, lng: -104.7556 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 3.5,
    center: centerAirport,
  });

  for (let i = 0; i < allAirportLocs.length; i++) {
    const airport = allAirportLocs[i];
    new google.maps.Marker({
      position: { lat: airport[1], lng: airport[2] },
      map,
      //icon: image,
      //shape: shape,
      title: airport[0],
    });
  }
}

//change map to respond to airport code search
function updateMap(code) {
  // The location of Denver, the center
  const centerAirport = { lat: 39.7783, lng: -104.7556 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 3.5,
    center: centerAirport,
  });

  for (let i = 0; i < allAirportLocs.length; i++) {
    const airport = allAirportLocs[i];
    if (airport[0] == code) {
      new google.maps.Marker({
        position: { lat: airport[1], lng: airport[2] },
        map,
        //icon: image,
        //shape: shape,
        title: airport[0],
      });
    }
  }
}
