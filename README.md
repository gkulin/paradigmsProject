# paradigmsProject
data source:  https://think.cs.vt.edu/corgis/datasets/json/airlines/airlines.json

# Complexity
The most complex part of our project was making sure our data has the necessary information. We did not originally have any location regarding the location of airports (which we needed for the Google Maps API). So, we added zip code information to each data element in our JSON. We also used another API (http://www.zippopotam.us/) to get the latitude and longitutde for our airports (which is how we are able to map them with the Google Maps API). We also iterated through out data to find the average delay for every airport, which involved some complexity in terms of parsing our data properly.

# REST API Table: 
| Command | Resource | Input Example | Output Exmaple | Behavior|
| ---     |  ------  |------ |------  |---------|
| GET   | /airports/   | -   |{“result”: “success”, “entries”: [“ATL”, “BOS”, “BWI”,...]} | Displays all airport codes in the data |
| GET   | /airports/:airport/:yr/:mo   | -   |{“result”: “success”, “Average Delay”: 46.0} | Displays average delay for a specific year and month at a specific airport | {}
| GET   | /airports/:airport   | -   |{“result”: “success”, “Average Delays by Month”: {“January”: 59.3, “February”: 57.6, “March”: 58.3}} | Displays the average delay for each month for a specific airport |
| GET   | /airports/ | - | [“ATL”, “BOS”, “BWI”, ...] | Displays all airport codes | 
| GET   | /airpport/:airport/:yr/:mo | - | "Flights": {"Cancelled": 216,"Delayed": 5843,"Diverted": 27,"On Time": 23974,"Total": 30060},"Minutes Delayed": {"Carrier": 61606,"Late Aircraft": 68335,"National Aviation System": 118831,"Security": 518,"Total": 268764,"Weather": 19474} | Displays stats about specific airport in certain month & year |
| POST   | /airports/:airport |{'Airport': {'Code': 'SBN', 'Name': 'South Bend, IN: South Bend International'}, 'Time': {'Label': '2020/01', 'Month': 1, 'Month Name': 'January', 'Year': 2020}, 'Statistics': {'# of Delays': {'Carrier': 1009, 'Late Aircraft': 1275,'National Aviation System': 3217, 'Security': 17, 'Weather': 328}, 'Carriers': {'Names': 'Allegiant, American Airlines, Delta, United', 'Total': 4}, 'Flights': {'Cancelled': 2, 'Delayed': 10, 'Diverted': 3, 'On Time': 1000, 'Total': 1015}, 'Minutes Delayed': {'Carrier': 500, 'Late Aircraft': 700, 'National Aviation System': 200, 'Security': 40, 'Total': 1740, 'Weather': 300}}} | {“result”: “success”} if the operation worked | Adds a new airport (or another entry for a new month/year for an existing airport) |
| PUT  | /airports/:airport/:yr/:mo  | {‘Minutes Delayed’ : {"Carrier": 61606, "Late Aircraft": 68335, "National Aviation System": 118831, "Security": 754, "Total": 269000, "Weather": 19474}} |{{“result”: “success”} if the operation worked | Updates the values for minutes delayed |
| DELETE | /airports/:airport/:yr/:mo | - | {“result”: “success”} if the operation worked | Deletes a specific entry for a specific airport|

